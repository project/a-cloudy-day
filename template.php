<?php
	/**
	 * $Id$
	 * A cloudy day
	 * Theme by carettedonny.be
	 */
	function phptemplate_feed_icon($url, $title) {
	  if ($image = theme('image', path_to_theme() . '/images/rss.png', t('Syndicate content'), $title)) {
	    return '<a href="'. check_url($url) .'" class="feed-icon">'. $image .'</a>';
	  }
	}
	
	function phptemplate_breadcrumb($breadcrumb) {
  		if (!empty($breadcrumb)) {
    		return '<div class="breadcrumb">'. implode(" <img src=\"".base_path().path_to_theme()."/images/list-item.gif\" alt=\"\" /> ", $breadcrumb) .'</div>';
  		}	
	}