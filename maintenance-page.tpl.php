<?php
	/**
	 * $Id$
	 * A cloudy day
	 * Theme by carettedonny.be
	 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $language->language;?>" lang="<?php echo $language->language;?>" dir="<?php echo $language->dir;?>">
	<head>
	    <title><?php echo $head_title;?></title>
		<?php echo $head;?>
	    <?php echo $styles;?>
	    <?php echo $scripts;?> 	
		<!--[if lt IE 7.]>
				<style type="text/css" media="screen">
					@import url(<?php print base_path() . path_to_theme() ?>/ie_styles.css);
				</style>
		<![endif]--> 
		<script type="text/javascript">
			<!--//--><![CDATA[//><!--
			Drupal.behaviors.tntIEFixes = function (context) {
  				if ($.browser.msie && ($.browser.version < 7)) {
    				$('#topmenu li').hover(function() {
      					$(this).addClass('hover');
      				}, function() {
        				$(this).removeClass('hover');
    				});
  				};
			};
			//--><!]]>
		</script>
	</head>
  	<body>
  		<div id="top">
  			<div id="topmenu">
  				<?php print menu_tree($menu_name = 'primary-links'); ?>
  			</div>
  		</div>
		<div id="header">
			<div id="headertitle">
				<h1><a href="<?php echo $front_page;?>" title="<?php echo t('Home') ?>"><?php echo $site_name;?></a></h1>
				<div class='site-slogan'>
					<?php echo $site_slogan ;?>
				</div>
			</div>
			<?php if ($logo) : ?>
				<div id="logo">
        			<a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><img src="<?php print($logo) ?>" alt="<?php print t('Home') ?>" border="0" /></a>
      			</div>
			<?php endif; ?>	
		</div><!-- /header -->
		<?php if ($show_messages && $messages): echo "<div id=\"messagebox\">".$messages."</div>"; endif; ?>				
		<div id="contentcontainer">
			<div id="container">
				<div id="contentleft">
					<div id="page">
						<?php echo $breadcrumb; ?>
			            <?php if ($mission): echo '<div id="mission">'. $mission .'</div>'; endif; ?>
			            <?php if ($tabs): echo '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
			            <?php if ($title): echo '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
			            <?php if ($tabs): echo '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
			            <?php if ($tabs2): echo '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
			            <?php echo $help; ?>
			            <?php echo $content ?>
						<div class="feedicons">
			            	<?php echo $feed_icons ?>
						</div>
					</div>
					<?php if ($footer): echo "<div id=\"footer\">".$footer."</div>"; endif;?>
				</div><!-- /contentleft -->
				<div id="contentright">
					<?php echo $right; ?>
				</div><!-- /contentright -->
			</div><!-- /container -->
		</div><!-- /contentcontainer -->
		<div id="bottompage">
			<div id="skyline">				
			</div>
			<div id="bottomtext">
				Theme designed by <a href="http://www.carettedonny.be" title="Donny Carette">Donny Carette</a> - Powered by <a href="http://www.drupal.org" title="Drupal">Drupal</a> - copyright &copy; <?php echo date("Y");?>
			</div>
		</div> 		
	<?php echo $closure ?>
	</body>
</html>