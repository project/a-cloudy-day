<?php
	/**
	 * $Id$
	 * A cloudy day
	 */
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
	<?php print $picture ?>
	<?php if ($submitted): ?>
	    <div class="date">
	    	<div class="textdate">
	      		<div class="day"><?php print format_date($created, 'custom', 'j'); ?></div>
	      		<div class="month"><?php print format_date($created, 'custom', 'M'); ?></div>
		  	</div>
	    </div>
	<?php endif; ?>
	<?php if ($page == 0): ?>
	  	<h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
	<?php endif; ?>

	<?php if ($submitted): ?>
	    <span class="submitted"><?php print $submitted; ?></span>
	<?php endif; ?>
	
	  <div class="content clear-block">
	  	<?php print $content ?>
	  </div>
	
	  <div class="clear-block">
	  	<div class="meta">
	    	<?php if ($taxonomy): ?>
	      		<div class="terms">Tags:<?php print $terms ?></div>
	    	<?php endif;?>
	    </div>
	
	    <?php if ($links): ?>
	    	<div class="links"><?php print $links; ?></div>
	    <?php endif; ?>
	</div>
</div>